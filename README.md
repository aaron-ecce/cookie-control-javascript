# Cookie Proxy

A Proxy for document.cookie to control which cookies are allowed to be created with a filter.

## Installation

```npm install cookie-control --save```

## Usage

```javascript
<script src="dist/cookieproxy.min.js"></script>
<script>
  CookieProxy.setFilter(function(cookie) {
  	return true; // Returning false will prevent the cookie from being set
  });
</script>
```

Make sure to include this script as early as possible to prevent other scripts from setting cookies before the proxy has been applied.

## License

MIT
