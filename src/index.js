'use strict';

/**
 * Grab the original document.cookie settter/getter object
 */
let cookieSetterGetter = Object.getOwnPropertyDescriptor(HTMLDocument.prototype, 'cookie') || Object.getOwnPropertyDescriptor(Document.prototype, 'cookie');

let createProxy = require('./proxy');
let shim = require('./shim');
function noop() {}

module.exports = function(filter) {
	let proxy = createProxy(cookieSetterGetter);
	shim(proxy);
};
