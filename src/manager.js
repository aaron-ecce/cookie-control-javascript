'use strict';

let setterFilter = function(value) {};

function setFilter(filter) {
	setterFilter = filter;
}

function getFilter() {
	return setterFilter;
}

module.exports = (function() {
	return {
		setFilter: setFilter,
		getFilter: getFilter
	}
})();
