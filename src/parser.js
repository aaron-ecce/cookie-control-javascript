'use strict';

/**
 * Breaks a cookie string into raw parts and decodes values
 * 
 * @param  {string} cookie Cookie string to split
 * @return {array}         Array of the split parts
 */
function cookieStringSplitter(cookie) {
	return cookie.split(';').map(function(result) {
  		return result.trim().split('=').map(decodeURIComponent);
  	});
}

/**
 * Parses a full cookie string and converts to an object
 * 
 * @param  {string} cookie The full cookie string from document.cookie
 * @return {object}        All cookies name => value pairs
 */
function parseFullCookieString(cookie) {
	return cookieStringSplitter(cookie).reduce(function(a, b) {
		try {
			a[b.name] = JSON.parse(b.value);
		} catch (e) {
			a[b.name] = b.value;
		}

		return a;
	}, {});
}

/**
 * Parses cookie value string into an object
 * 
 * @param  {string} value The individual cookie value (document.cookie setter)
 * 
 * @return {object}       The cookie stirng parsed into an object
 */
function parseCookieValue(value) {
	return cookieStringSplitter(value).reduce(function(accumulator, value) {

  		// If name is not set, we are on first iteration
  		if (!accumulator.name) {
  			accumulator.name = value[0];

  			try {
  				accumulator.value = JSON.parse(value[1]);
  			} catch(e) {
  				accumulator.value = value[1];
  			}

  			return accumulator;
  		}

  		// Other properties for the cookie string
  		try {
  			accumulator[value[0]] = JSON.parse(value[1]);
  		} catch (e) {
  			accumulator[value[0]] = value[1];
  		}

  		return accumulator;
  	}, {});
}

module.exports = {
	parseCookies: parseFullCookieString,
	parse: parseCookieValue
};
