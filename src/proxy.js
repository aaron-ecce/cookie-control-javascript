'use strict';

let parser = require('./parser');
let manager = require('./manager');

module.exports = function createProxy(cookieSetterGetter) {

	return (function() {

		let proxy = {
			subject: cookieSetterGetter
		};

		proxy.set = function(value) {
			let filter = manager.getFilter();
			if (filter(parser.parse(value))) {
				document.cookieProxy = value;
			}
		};

		proxy.get = function() {
			return document.cookieProxy;
		};

		/**
		 * Apply the original cookie setter and getter to the proxy to actual save the cookies
		 */
		document.__defineGetter__('cookieProxy', cookieSetterGetter.get);
        document.__defineSetter__('cookieProxy', cookieSetterGetter.set);

		return proxy;
	}.bind(document))();
};
