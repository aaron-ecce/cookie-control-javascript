'use strict';

module.exports = function shimAssign(proxy) {
	proxy.__defineGetter__('cookie', proxy.subject.get);
	proxy.__defineSetter__('cookie', proxy.subject.set);

	Object.defineProperty(document, 'cookie', proxy);
};
