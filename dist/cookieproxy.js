(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

let proxy = require('./');
let manager = require('./manager');

window['CookieProxy'] = manager;
proxy();

},{"./":2,"./manager":3}],2:[function(require,module,exports){
'use strict';

/**
 * Grab the original document.cookie settter/getter object
 */
let cookieSetterGetter = Object.getOwnPropertyDescriptor(HTMLDocument.prototype, 'cookie') || Object.getOwnPropertyDescriptor(Document.prototype, 'cookie');

let createProxy = require('./proxy');
let shim = require('./shim');
function noop() {}

module.exports = function(filter) {
	let proxy = createProxy(cookieSetterGetter);
	shim(proxy);
};

},{"./proxy":5,"./shim":6}],3:[function(require,module,exports){
'use strict';

let setterFilter = function(value) {};

function setFilter(filter) {
	setterFilter = filter;
}

function getFilter() {
	return setterFilter;
}

module.exports = (function() {
	return {
		setFilter: setFilter,
		getFilter: getFilter
	}
})();

},{}],4:[function(require,module,exports){
'use strict';

/**
 * Breaks a cookie string into raw parts and decodes values
 * 
 * @param  {string} cookie Cookie string to split
 * @return {array}         Array of the split parts
 */
function cookieStringSplitter(cookie) {
	return cookie.split(';').map(function(result) {
  		return result.trim().split('=').map(decodeURIComponent);
  	});
}

/**
 * Parses a full cookie string and converts to an object
 * 
 * @param  {string} cookie The full cookie string from document.cookie
 * @return {object}        All cookies name => value pairs
 */
function parseFullCookieString(cookie) {
	return cookieStringSplitter(cookie).reduce(function(a, b) {
		try {
			a[b.name] = JSON.parse(b.value);
		} catch (e) {
			a[b.name] = b.value;
		}

		return a;
	}, {});
}

/**
 * Parses cookie value string into an object
 * 
 * @param  {string} value The individual cookie value (document.cookie setter)
 * 
 * @return {object}       The cookie stirng parsed into an object
 */
function parseCookieValue(value) {
	return cookieStringSplitter(value).reduce(function(accumulator, value) {

  		// If name is not set, we are on first iteration
  		if (!accumulator.name) {
  			accumulator.name = value[0];

  			try {
  				accumulator.value = JSON.parse(value[1]);
  			} catch(e) {
  				accumulator.value = value[1];
  			}

  			return accumulator;
  		}

  		// Other properties for the cookie string
  		try {
  			accumulator[value[0]] = JSON.parse(value[1]);
  		} catch (e) {
  			accumulator[value[0]] = value[1];
  		}

  		return accumulator;
  	}, {});
}

module.exports = {
	parseCookies: parseFullCookieString,
	parse: parseCookieValue
};

},{}],5:[function(require,module,exports){
'use strict';

let parser = require('./parser');
let manager = require('./manager');

module.exports = function createProxy(cookieSetterGetter) {

	return (function() {

		let proxy = {
			subject: cookieSetterGetter
		};

		proxy.set = function(value) {
			let filter = manager.getFilter();
			if (filter(parser.parse(value))) {
				document.cookieProxy = value;
			}
		};

		proxy.get = function() {
			return document.cookieProxy;
		};

		/**
		 * Apply the original cookie setter and getter to the proxy to actual save the cookies
		 */
		document.__defineGetter__('cookieProxy', cookieSetterGetter.get);
        document.__defineSetter__('cookieProxy', cookieSetterGetter.set);

		return proxy;
	}.bind(document))();
};

},{"./manager":3,"./parser":4}],6:[function(require,module,exports){
'use strict';

module.exports = function shimAssign(proxy) {
	proxy.__defineGetter__('cookie', proxy.subject.get);
	proxy.__defineSetter__('cookie', proxy.subject.set);

	Object.defineProperty(document, 'cookie', proxy);
};

},{}]},{},[1]);
